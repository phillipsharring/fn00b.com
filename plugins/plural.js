import Vue from 'vue'
import pluralize from 'pluralize'

Vue.filter('plural', (word, count) => {
  return pluralize(word, count)
})
