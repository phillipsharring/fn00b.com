---
content-type: page-section
title: Career Paths & Categories
---
Here are resources organized by career path. Careers are interesting, and I don’t think anyone really starts off knowing exactly what they want to do.

One of my guides for life, is a quote by Steve Jobs:

> "Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma- which is living with the results of other people's thinking. Don't let the noise of other's opinions drown out your own inner voice. And most important, have the courage to follow your heart and your intuition. They somehow know what you truly want to become. Everything else is secondary."
