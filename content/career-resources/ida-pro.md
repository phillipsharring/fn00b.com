---
content-type: career-resource
title: IDA Pro
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://www.hex-rays.com/products/ida/
---
A Disassembler and Debugger