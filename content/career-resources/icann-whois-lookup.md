---
content-type: career-resource
title: ICANN WHOIS Lookup
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://lookup.icann.org/
---
Domain Name Registration Data Lookup