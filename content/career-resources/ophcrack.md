---
content-type: career-resource
title: Ophcrack
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://ophcrack.sourceforge.io/
---
Ophcrack is a free Windows password cracker based on rainbow tables.