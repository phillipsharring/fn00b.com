---
content-type: career-resource
title: MalShare
category: Malware Analysis
categorySlug: malware-analysis
resourceType: Website
resourceTypeSlug: website
url: https://malshare.com/
---
A free Malware repository providing researchers access to samples, malicious feeds, and Yara results.