---
content-type: career-resource
title: OWASP Foundation
category: Security Information
categorySlug: security-information
resourceType: Website
resourceTypeSlug: website
url: https://www.owasp.org/index.php/Main_Page
---
The Open Web Application Security Project (OWASP) is a 501(c)(3) worldwide not-for-profit charitable organization focused on improving the security of software.