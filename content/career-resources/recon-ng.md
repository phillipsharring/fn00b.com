---
content-type: career-resource
title: Recon-ng
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://github.com/lanmaster53/recon-ng
---
Open Source Intelligence gathering tool aimed at reducing the time spent harvesting information from open sources.