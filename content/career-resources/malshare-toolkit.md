---
content-type: career-resource
title: MalShare Toolkit
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://github.com/Malshare/MalShare-Toolkit
---
Set of tools for interacting with Malshare