---
content-type: career-resource
title: RainbowCrack
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: http://project-rainbowcrack.com/
---
RainbowCrack is a general propose implementation of Philippe Oechslin's faster time-memory trade-off technique. It crack hashes with rainbow tables.