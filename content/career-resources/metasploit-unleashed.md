---
content-type: career-resource
title: Metasploit Unleashed
category: Pen Testing
categorySlug: pen-testing
resourceType: Training
resourceTypeSlug: training
url: https://www.offensive-security.com/metasploit-unleashed/
---
Metasploit Unleashed – Free Ethical Hacking Course by Offensive Security