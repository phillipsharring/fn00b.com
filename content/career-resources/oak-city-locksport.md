---
content-type: career-resource
title: Oak City LockSport
category: RTP Security Groups
categorySlug: rtp-security-groups
resourceType: Meetup
resourceTypeSlug: meetup
url: https://www.meetup.com/Oak-City-Locksport/
---
Oak City Locksport is the original Raleigh area lockpicking group, founded in 2010 by Katie (aka "squ33k"), and led today by squ33k and Patrick (aka "unregistered436 (https://twitter.com/unregistered436)").