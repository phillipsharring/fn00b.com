---
content-type: career-resource
title: The Scrap Value of a Hacked PC, Revisited
category: Security Articles and Blogs
categorySlug: security-articles-and-blogs
resourceType: Article
resourceTypeSlug: article
url: https://krebsonsecurity.com/2012/10/the-scrap-value-of-a-hacked-pc-revisited/
---
A Krebs article