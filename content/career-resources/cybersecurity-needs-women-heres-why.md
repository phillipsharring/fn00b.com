---
content-type: career-resource
title: "Cybersecurity Needs Women: Here's Why"
category: Women's Resources
categorySlug: womens-resources
resourceType: Article
resourceTypeSlug: article
url: https://www.forbes.com/sites/laurencebradford/2018/10/18/cybersecurity-needs-women-heres-why/#fd7061147e8e
---
Work culture can get stuck in a self-perpetuating cycle of unconscious bias.
