---
content-type: career-resource
title: Ncat
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://nmap.org/ncat/
---
Netcat (often abbreviated to nc) is a computer networking utility for reading and writing from and to network connections using Transmission Control Protocol (TCP) or User Datagram Protocol (UDP).