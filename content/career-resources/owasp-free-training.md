---
content-type: career-resource
title: OWASP Free Training
category: General Training
categorySlug: general-training
resourceType: Training
resourceTypeSlug: training
url: https://www.owasp.org/index.php/Education/Free_Training
---
The following courses either have been offered or are being offered free of charge courtesy of the trainers and the OWASP Foundation to anyone interested in learning about application security.