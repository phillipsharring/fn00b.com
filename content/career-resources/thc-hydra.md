---
content-type: career-resource
title: THC Hydra
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://github.com/vanhauser-thc/thc-hydra
---
This tool is a proof of concept code, to give researchers and security consultants the possibility to show how easy it would be to gain unauthorized access from remote to a system.