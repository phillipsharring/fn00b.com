---
content-type: career-resource
title: CurrPorts
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://www.nirsoft.net/utils/cports.html
---
CurrPorts is network monitoring software that displays the list of all currently opened TCP/IP and UDP ports on your local computer.