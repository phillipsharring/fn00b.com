---
content-type: career-resource
title: TCPView
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://docs.microsoft.com/en-us/sysinternals/downloads/tcpview
---
TCPView is a Windows program that will show you detailed listings of all TCP and UDP endpoints on your system, including the local and remote addresses and state of TCP connections.