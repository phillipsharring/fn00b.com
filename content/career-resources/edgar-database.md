---
content-type: career-resource
title: Edgar Database
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://www.sec.gov/edgar
---
Securities and Exchange Commission Edgar Database