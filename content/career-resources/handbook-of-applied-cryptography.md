---
content-type: career-resource
title: Handbook of Applied Cryptography
category: Cryptography
categorySlug: cryptography
resourceType: Book
resourceTypeSlug: book
url: https://cacr.uwaterloo.ca/hac/
---
A comprehensive source from which to learn cryptography, serving both students and instructors