---
content-type: career-resource
title: Building Your Cisco Home Lab
category: DIY Labs
categorySlug: diy-labs
resourceType: Training
resourceTypeSlug: training
url: https://www.cbtnuggets.com/blog/2017/07/building-your-cisco-home-lab/
---
Building a Cisco home lab can seem a little overwhelming the first time you decide to take the plunge.