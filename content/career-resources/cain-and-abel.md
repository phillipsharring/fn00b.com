---
content-type: career-resource
title: Cain and Abel
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: http://www.oxid.it/cain.html
---
Windows-only password recovery tool handles an enormous variety of tasks