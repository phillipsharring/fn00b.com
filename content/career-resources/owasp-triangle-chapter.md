---
content-type: career-resource
title: OWASP Triangle Chapter
category: RTP Security Groups
categorySlug: rtp-security-groups
resourceType: Meetup
resourceTypeSlug: meetup
url: https://www.meetup.com/owasptriangle/
---
OWASP Triangle Chapter. Free to join, open to all. We meet to discuss & demonstrate primarily web and mobile vulnerabilities, tools & solutions.