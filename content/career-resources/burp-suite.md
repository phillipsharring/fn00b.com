---
content-type: career-resource
title: Burp Suite
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://portswigger.net/burp
---
Toolkit for investigating web security