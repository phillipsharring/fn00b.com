---
content-type: career-resource
title: Build your own cyber lab at home
category: DIY Labs
categorySlug: diy-labs
resourceType: Training
resourceTypeSlug: training
url: https://www.udemy.com/build-your-own-cyber-lab-at-home/
---
Learn new software and hacking techniques with your own lab