---
content-type: career-resource
title: Raleigh 2600
category: RTP Security Groups
categorySlug: rtp-security-groups
resourceType: Meetup
resourceTypeSlug: meetup
url: http://www.nc2600.org/meetings.html
---
2600 meetings are held on the first Friday of every month
@Morning Times- 7pm -ish