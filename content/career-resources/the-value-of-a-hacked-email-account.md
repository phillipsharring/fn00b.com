---
content-type: career-resource
title: The Value of a Hacked Email Account
category: Security Articles and Blogs
categorySlug: security-articles-and-blogs
resourceType: Article
resourceTypeSlug: article
url: https://krebsonsecurity.com/2013/06/the-value-of-a-hacked-email-account/
---
A Krebs article