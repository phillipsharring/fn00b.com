---
content-type: career-resource
title: OSRFramework
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://github.com/i3visio/osrframework
---
OSRFramework, the Open Sources Research Framework is a AGPLv3+ project by i3visio focused on providing API and tools to perform more accurate online researches.