---
content-type: career-resource
title: How to Make Your Own Penetration Testing Lab
category: DIY Labs
categorySlug: diy-labs
resourceType: Training
resourceTypeSlug: training
url: https://resources.infosecinstitute.com/how-to-make-your-own-penetration-testing-lab/#gref
---
Instructions on how to build your own pen testing lab