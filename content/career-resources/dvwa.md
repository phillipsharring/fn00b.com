---
content-type: career-resource
title: DVWA
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: http://www.dvwa.co.uk/
---
Damn Vulnerable Web App (DVWA) is a PHP/MySQL web application that is damn vulnerable