---
content-type: career-resource
title: NMap
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://nmap.org/
---
Nmap ("Network Mapper") is a free and open source (license) utility for network discovery and security auditing