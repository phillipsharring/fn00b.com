---
content-type: career-resource
title: Raleigh ISSA
category: RTP Security Groups
categorySlug: rtp-security-groups
resourceType: Meetup
resourceTypeSlug: meetup
url: https://raleigh.issa.org/
---
Raleigh's Information System Security Association promoting education, career services, CEU's, Community and networking.
First Thursday of every month @RTPHQ's