---
content-type: career-resource
title: EC-Council
category: General Training
categorySlug: general-training
resourceType: Training
resourceTypeSlug: training
url: https://www.eccouncil.org/
---
EC-Council is the world’s largest cyber security technical certification body