---
content-type: career-resource
title: Bug Crowd University
category: General Training
categorySlug: general-training
resourceType: Training
resourceTypeSlug: training
url: https://github.com/bugcrowd/bugcrowd_university
---
Bugcrowd University is a free and open source project to help level-up our security researchers.