---
content-type: career-resource
title: Offensive Security
category: Pen Testing
categorySlug: pen-testing
resourceType: Training
resourceTypeSlug: training
url: https://www.offensive-security.com/
---
Industry-Defining Penetration Testing Courses and Certifications
For Information Security Professionals