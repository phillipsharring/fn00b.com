---
content-type: career-resource
title: Federal Virtual Training Environment
category: General Training
categorySlug: general-training
resourceType: Training
resourceTypeSlug: training
url: https://niccs.us-cert.gov/training/federal-virtual-training-environment-fedvte
---
The National Initiative for Cybersecurity Careers and Studies (NICCS) is the premier online resource for cybersecurity training.