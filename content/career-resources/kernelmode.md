---
content-type: career-resource
title: KernelMode
category: Malware Analysis
categorySlug: malware-analysis
resourceType: Website
resourceTypeSlug: website
url: https://www.kernelmode.info/forum/
---
A forum for reverse engineering, OS internals and malware analysis