---
content-type: career-resource
title: Diana Initiative
category: Women's Resources
categorySlug: womens-resources
resourceType: Website
resourceTypeSlug: website
url: https://www.dianainitiative.org/
---
Our mission is to:
Encourage diversity and support women who want to pursue careers in information security,
Promote diverse and supportive workplaces, and help change workplace cultures.