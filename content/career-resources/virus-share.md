---
content-type: career-resource
title: Virus Share
category: Malware Analysis
categorySlug: malware-analysis
resourceType: Website
resourceTypeSlug: website
url: https://virusshare.com/
---
VirusShare.com is a repository of malware samples to provide security researchers, incident responders, forensic analysts, and the morbidly curious access to samples of live malicious code.