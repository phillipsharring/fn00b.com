---
content-type: career-resource
title: Maltego
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://www.maltego.com/
---
An open source intelligence (OSINT) and graphical link analysis tool for gathering and connecting information for investigative tasks.