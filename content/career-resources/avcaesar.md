---
content-type: career-resource
title: AVCaesar
category: Malware Analysis
categorySlug: malware-analysis
resourceType: Website
resourceTypeSlug: website
url: https://avcaesar.malware.lu/
---
AVCaesar is a malware analysis engine and repository.
Your suspicious files can be analysed by a set of antivirus.