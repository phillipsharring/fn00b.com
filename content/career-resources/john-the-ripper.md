---
content-type: career-resource
title: John the Ripper
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://www.openwall.com/john/
---
John the Ripper is a fast password cracker, currently available for many flavors of Unix, macOS, Windows, DOS, BeOS, and OpenVMS