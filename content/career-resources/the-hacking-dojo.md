---
content-type: career-resource
title: The Hacking Dojo
category: Pen Testing
categorySlug: pen-testing
resourceType: Training
resourceTypeSlug: training
url: http://hackingdojo.com/
---
Training ground for professional hackers