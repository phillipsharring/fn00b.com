---
content-type: career-resource
title: RTP Security + Beers
category: RTP Security Groups
categorySlug: rtp-security-groups
resourceType: Meetup
resourceTypeSlug: meetup
url: https://www.meetup.com/RTP-Security-Beers/
---
Information security professionals meeting informally to talk shop over a beer (or beverage of choice)