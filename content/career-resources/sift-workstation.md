---
content-type: career-resource
title: SIFT Workstation
category: Tools
categorySlug: tools
resourceType: Website
resourceTypeSlug: website
url: https://digital-forensics.sans.org/community/downloads
---
The SIFT Workstation is a group of free open-source incident response and forensic tools designed to perform detailed digital forensic examinations in a variety of settings.