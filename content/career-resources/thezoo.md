---
content-type: career-resource
title: theZoo
category: Malware Analysis
categorySlug: malware-analysis
resourceType: Website
resourceTypeSlug: website
url: https://github.com/ytisf/theZoo
---
A repository of LIVE malwares for your own joy and pleasure. theZoo is a project created to make the possibility of malware analysis open and available to the public. https://thezoo.morirt.com