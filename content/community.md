---
content-type: full-page
title: Community
description: Fn00b's Security Community page
---
# Community

This is a page dedicated to the security community. I hope to build it out to help people from all over find local resources in their community. This also may take some time and rely on input from all the communities, BUT it’s a grand idea that can help those looking to get into security or find other groups and conferences in their area.
