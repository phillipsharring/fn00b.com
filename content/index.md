---
content-type: page-section
---
A frickin' n00b. A functional n00b. A fantastic n00b. Always learning, progressing, exploring, tinkering, and checking out new stuff.
 Always willing to teach, help, and hop in!

I call myself Fn00b as an internal reminder to never stop learning, and as encouragement to others who may be new. You are not alone, we were all new too, and the more we expand beyond our comfort zone, the more often we are n00bs. So, don’t feel discouraged about how new you may be, but pat yourself on the back for jumping into something new!

This website is dedicated to sharing information, and to all the mentors and mentees that have been a part of my journey. The website is essentially living, but I might not update it as often.

The original idea was to create a bookmarking tool as I come across information that I want to save. Several people had asked me for resource information and I usually would send them a list of bookmarks. I hope this is a little easier than all that. I also hope to add some personal insight at some point somewhere on the site. It’s a work in progress amongst all the other items on my constant agenda. I’m sure you understand.
