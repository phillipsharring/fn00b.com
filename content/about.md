---
content-type: full-page
title: About
description: All about Fn00b
---
<img src="/img/fn00b.jpg" alt="Fn00b in her element, soldering a circuit" class="right">

# About

I am a passionate and dedicated security engineer, who engages challenging situations with confidence, and never stops learning, teaching, or sharing information.

Ultimately, learning is my passion. I have enjoyed working with technology for as long as I can remember. In the third grade I went on a class field trip to the library, to talk about computers. When we got there, we were shown to an area where there were several desktop computers taken apart with all the pieces laid out on the tables for us to see. I was immediately amazed and overwhelmed with curiosity. To my imagination, the transistors and resistors on the motherboard were little buildings. The traces were roads. The fan was a stadium. AND this was the COOLEST micromachines city EVER.

I wanted to know everything about this machine. What can it do? How does it work? How could I use it?
That curiosity and passion for technology has followed me throughout my life. I am still learning, and there is always something new for me to pursue. I will always be a n00b in something because I am curious and always looking to expanding on the foundation of knowledge that I have already acquired.

While some may feel like calling myself a n00b is a bad thing, and maybe not what I should be presenting myself as, the truth is, I will never know everything, but I am going to do what I can to learn as much as I can, and share as much as I can along the way.

I regularly help run and plan security conferences, have spoken at several, and am involved with the security community. If you see me around, feel free to say hi. If you are interested in learning more about our community, want to reach out to me directly, or just want to keep up with me, feel free to follow me on twitter, or shoot me a message.
-Fn00b

## Contact Information

<a href="https://twitter.com/Fn00b">Follow @Fn00b</a>  
Email: Fn00b[at]Fn00b.com
