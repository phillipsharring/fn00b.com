---
content-type: full-page
title: Credits
description: Credits for the Fn00b.com website with Special Thanks to the Security Community
updated: Thu, 4 Mar, 2021
---
# Credits

## Header Image

The home page header of this site uses [this photo](https://unsplash.com/photos/OcALKeCIKBg)
by [Nick Fewings](https://unsplash.com/@jannerboy62) from Unsplash, with thanks.

## Icons

This site uses Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [flaticon.com](https://www.flaticon.com/)

## Website

Special thanks to my wonderful husband for actively engaging in helping me achieve my dreams. He has dedicated countless hours and nights helping my vision come to life. His [own website](https://phillipharrington.com) sucks in comparison. (He put that last part in there, it made me laugh so I wanted to keep it.)

## Special Thanks

I also want to thank many of the security and IT professionals that came before me, laid the path that we walk on today, and who have guided and encouraged me along the way. Thank you all.
