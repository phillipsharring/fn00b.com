export default async function ({ $configApi, $config, store, $content }) {
  const careerResources = await store.dispatch('content/getCareerResources', $content)
  store.dispatch('menus/setMenuItemsFromStore', careerResources)
}
