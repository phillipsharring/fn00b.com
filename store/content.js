export const state = () => ({
  careerResources: null,
})

export const getters = {
  careerResources: state => state.careerResources,

  careerResourcesByType: (state) => (type) => state.careerResources[type],
}

export const mutations = {
  setCareerResources(state, careerResources) {
    state.careerResources = careerResources;
  },
}

export const actions = {
  getCareerResources({ getters, commit, dispatch }, $content) {
    const careerResources = getters.careerResources

    if (careerResources) {
      return new Promise(resolve => resolve(careerResources))
    }

    return $content('career-resources')
      .only(['slug', 'title', 'category', 'categorySlug', 'resourceType', 'resourceTypeSlug'])
      .where({ 'content-type': 'career-resource' })
      .fetch()
      .then(contents => {
        const careerResources = {
          categories: {},
          resourceTypes: {},
        }

        contents.forEach(content => {
          const { categorySlug, resourceTypeSlug } = content;

          if (!careerResources.categories.hasOwnProperty(categorySlug)) {
            careerResources.categories[categorySlug] = {
              label: content.category,
              slug: categorySlug,
              links: [],
            };
          }

          if (!careerResources.resourceTypes.hasOwnProperty(resourceTypeSlug)) {
            careerResources.resourceTypes[resourceTypeSlug] = {
              label: content.resourceType,
              slug: resourceTypeSlug,
              links: [],
            };
          }

          const link = { label: content.title, slug: content.slug }

          careerResources.categories[categorySlug].links.push(link);
          careerResources.resourceTypes[resourceTypeSlug].links.push(link);
        });

        commit('setCareerResources', careerResources)

        return careerResources
      })
  },
}
