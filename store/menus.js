export const state = () => ({
  menuItems: {
    main: {
      openDropdown: null,
      items: [
        {
          label: 'Career Paths & Categories',
          slug: 'category',
          dropdownMenu: {
            route: 'category-slug',
            links: [],
          },
        },
        {
          label: 'Resource Types',
          slug: 'resource-types',
          dropdownMenu: {
            route: 'resource-types-slug',
            links: [],
          },
        },
        {
          label: 'Community',
          route: 'slug',
          params: { slug: 'community' },
        },
        {
          label: 'Speaking Engagements',
          route: 'slug',
          params: { slug: 'speaking-engagements' },
        },
        {
          label: 'About',
          route: 'slug',
          params: { slug: 'about' },
        },
      ],
    },
    footer: {
      items: [
        {
          label: 'Career Paths & Categories',
          route: 'career-paths',
        },
        {
          label: 'Resource Types',
          route: 'resource-types',
        },
        {
          label: 'Community',
          route: 'slug',
          params: { slug: 'community' },
        },
        {
          label: 'Speaking Engagements',
          route: 'slug',
          params: { slug: 'speaking-engagements' },
        },
        {
          label: 'About',
          route: 'slug',
          params: { slug: 'about' },
        },
      ],
    }
  },
})

export const getters = {
  items: (state) => (menu) => state.menuItems[menu].items
}

export const mutations = {
  setDropdownLinks(state, payload) {
    const { menu, slug, links } = payload;
    const items = state.menuItems[menu].items

    const newItems = items.map(item => {
      if (item.dropdownMenu && item.slug === slug) {
        item.dropdownMenu.links = links
      }

      return item
    })

    state.menuItems[menu].items = newItems
  },
}

export const actions = {
  setMenuItemsFromStore({ commit }, careerResources) {
    const categoryLinks = [];

    for (const [key, category] of Object.entries(careerResources.categories)) {
      categoryLinks.push(
        { label: category.label, slug: category.slug }
      )
    }

    const resourceTypeLinks = [];

    for (const [key, resourceType] of Object.entries(careerResources.resourceTypes)) {
      resourceTypeLinks.push(
        { label: resourceType.label, slug: resourceType.slug }
      )
    }

    commit('setDropdownLinks', {
      menu: 'main',
      slug: 'category',
      links: categoryLinks
    })

    commit('setDropdownLinks', {
      menu: 'main',
      slug: 'resource-types',
      links: resourceTypeLinks
    })
  }
}
